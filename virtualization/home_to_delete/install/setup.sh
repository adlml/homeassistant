#!/bin/bash
# command to convert to unix bash script:
# - TYPE setupw.sh | MORE /P > setup.sh
cd /home/install

# copy public and private key
mkdir ~/.ssh
chmod 700 ~/.ssh
cp id_rsa ~/.ssh/id_rsa
cp id_rsa.pub ~/.ssh/id_rsa.pub

# set correct permissions
chmod 400 ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa.pub

# install nextcloud insecure certificate
#cp nextcloud.meier-lossburg.de.crt /usr/share/ca-certificates/nextcloud.meier-lossburg.de.crt
#update-ca-certificates
#ln -s /usr/share/ca-certificates/nextcloud.meier-lossburg.de.crt 09337d67.0

# copy darkflow stuff
#cp -R darkflow/ ../darkflow/
#cp darkflow/yolo.py /usr/local/lib/python3.5/site-packages/homeassistant/components/image_processing/yolo.py
#cp darkflow/yolo.py /usr/src/app/homeassistant/components/image_processing/yolo.py

#set the hosts
touch ~/.ssh/known_hosts
ssh-keyscan -p 50683 -H 192.168.10.11 >> ~/.ssh/known_hosts 
