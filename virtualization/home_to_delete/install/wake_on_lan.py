"""
Support for wake on lan.

For more details about this platform, please refer to the documentation at
https://home-assistant.io/components/switch.wake_on_lan/
"""

"""
    FIX Chris
    to turn of a computer openssh command is send to shutdown the device
    Z83 replaced do nothing

    WINDOWS tutorial ---------------------------------------:
        - install bitvisessh server
        - open bitvisessh server control Panel
        - add public key to current user z.b chris
        - add incoming custom firewall rule allowing icmp_v4 from
            foreign host: 192.168.0.x
        - open port 22 sshd on firewall for foreign host: 192.168.0.x

    WINDOWS WOL tutorial------------------------------------:
        - Motherboard BIOS:
            - LAN Option ROM = Enabled
            - EuP 2013 = Disabled
            - Resume by PCI-E device = Enabled
        - Windows 10 Settings:
            Eigenschaften von Lan Adapter:
            - Allow this device to wake the computer = ON
            - Only allow a magic packet to wake the computer = ON
            - Energy Efficient Ethernet = Disabled
            - Shutdown Wake Up = Enabled
            - Wake on Magic Packet = Enabled
            Energieoptionen
            - netzschalter/ schnellstart deaktivieren
            - auf maximale energie schalten!

    PFSENSE tutorial----------------------------------------:
        - create new user ha-shutdwon from web-interface
        - copy the public key into the field
        - add him to the group admin (or sudoerss TODO)
        - install package sudo if not done
        - in sudo tick following:
            [ha-shutdown][ALL] NOPASSWD [ALL]
        - ready

    NORM. LINUX host tutorial-------------------------------:
        login to remote host and create user: ha-shutdown
         - useradd -h  ha-shutdown
         - passwd ha-shutdown PASSWORT
        then remove password prompt
        - visudo
        add line
        - ha-shutdown ALL=(ALL) NOPASSWD: ALL

        copy ssh key to user:
        if not existent create dir:
            mkdir ~/.ssh
            chmod 700 ~/.ssh
        manual:
            copy with sudo nano into file .ssh/authorized_keys
        command: #doesn't work
            ssh-copy-id -i id_rsa.pub ha-shutdown@192.168.0.5
        alternative
            cat ~/.ssh/id_rsa.pub | ssh user@remote_host \
            'cat >> /home/user/.ssh/authorized_keys

    -----------------------------------
    on homeautomatino server
    ssh -i ~/.ssh/id_rsa ha-shutdown@192.168.0.5 command

"""
import logging
import platform
import subprocess as sp
import subprocess
import voluptuous as vol
import time

from homeassistant.components.switch import (SwitchDevice, PLATFORM_SCHEMA)
import homeassistant.helpers.config_validation as cv
from homeassistant.const import (CONF_HOST, CONF_NAME)

REQUIREMENTS = ['wakeonlan==0.2.2']

_LOGGER = logging.getLogger(__name__)

CONF_MAC_ADDRESS = 'mac_address'
DEFAULT_NAME = 'Wake on LAN'
CONF_OS = 'os'
CONF_SSH_NAME = 'ssh_name'
DEFAULT_PING_TIMEOUT = 1

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    vol.Required(CONF_MAC_ADDRESS): cv.string,
    vol.Required(CONF_OS): cv.string,
    vol.Optional(CONF_SSH_NAME, default='ha-server'): cv.string,
    vol.Optional(CONF_HOST): cv.string,
    vol.Optional(CONF_NAME, default=DEFAULT_NAME): cv.string,
})


def setup_platform(hass, config, add_devices_callback, discovery_info=None):
    """Add wake on lan switch."""
    name = config.get(CONF_NAME)
    host = config.get(CONF_HOST)
    os = config.get(CONF_OS)
    ssh_name = config.get(CONF_SSH_NAME)
    mac_address = config.get(CONF_MAC_ADDRESS)
    # store the loaded stuff from the conf in the __init__
    add_devices_callback([WOLSwitch(hass, name, host, os, ssh_name, mac_address)])


class WOLSwitch(SwitchDevice):
    """Representation of a wake on lan switch."""

    def __init__(self, hass, name, host, os, ssh_name, mac_address):
        """Initialize the WOL switch."""
        from wakeonlan import wol
        self._hass = hass
        self._name = name
        self._os = os # linux | freebsd | windows
        self._ssh_name = ssh_name
        self._host = host
        self._mac_address = mac_address
        self._state = False
        self._wol = wol
        self.update()

    @property
    def should_poll(self):
        """Poll for status regularly."""
        return True

    @property
    def is_on(self):
        """Return true if switch is on."""
        return self._state

    @property
    def name(self):
        """The name of the switch."""
        return self._name

    def turn_on(self):
        """Turn the device on."""
        self._wol.send_magic_packet(self._mac_address)
        self.update_ha_state()

    def ssh_into_and_shutdown(self):
        cmd = "ssh -i ~/.ssh/id_rsa %s@%s "%(self._ssh_name, self._host)
        if (self._os == "linux"):
            cmd += "\'sudo shutdown -p now\'"
        elif(self._os == "freebsd"):
            cmd += "\'sudo shutdown -p now\'"
        elif(self._os == "windows"):
            cmd += "\'shutdown -s -t 0\'"
        else:
            print("something went terribly wrong")
        proc = subprocess.Popen("exec " + cmd, shell=True)
        time.sleep(1)
        proc.kill()

    #proved working
    def turn_off(self):
        self.ssh_into_and_shutdown()
        self.update_ha_state()


    def update(self):
        """Check if device is on and update the state."""
        if platform.system().lower() == "windows":
            ping_cmd = "ping -n 1 -w {} {}"\
                .format(DEFAULT_PING_TIMEOUT * 1000, self._host)
        else:
            ping_cmd = "ping -c 1 -W {} {}"\
                .format(DEFAULT_PING_TIMEOUT, self._host)

        status = sp.getstatusoutput(ping_cmd)[0]

        self._state = not bool(status)
