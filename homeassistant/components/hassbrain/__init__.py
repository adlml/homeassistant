"""Example Load Platform integration."""
import logging

from homeassistant.components import websocket_api
from homeassistant.components.ebox.sensor import PERCENT
from homeassistant.core import callback
from homeassistant.helpers.entity import Entity
from homeassistant.helpers.entity_component import EntityComponent
from homeassistant.helpers.typing import HomeAssistantType
_LOGGER = logging.getLogger(__name__)

import voluptuous as vol

DOMAIN = 'hassbrain'
_UNDEF = 'undefiniert'

async def async_setup(hass, config):
    """Your controller/hub specific code."""
    # Data that you want to share with your platforms
    hass.data[DOMAIN] = {
      'temperature': 23
    }

    component = EntityComponent(_LOGGER, DOMAIN, hass)
    manager = hass.data[DOMAIN] = HassBrainManager(hass, component)
    await manager.async_initialize()

    websocket_api.async_register_command(hass, ws_list_devices)
    websocket_api.async_register_command(hass, ws_create_device)
    websocket_api.async_register_command(hass, ws_update_device)
    websocket_api.async_register_command(hass, ws_delete_device)
    return True

CONF_NAME = 'name'
CONF_STATE = 'state'

class HassBrainManager():
    def __init__(self, hass: HomeAssistantType, component: EntityComponent):
        self.hass = hass
        self.component = component


    async def async_initialize(self):
        pass
        entities = []
        conf = [{
            CONF_NAME: 'test_dev',
            CONF_STATE: 20 },
            {
            CONF_NAME: 'test_dev2',
            CONF_STATE: 40
            }
        ]
        entities.append(HassBrainSensor(conf[0]))
        entities.append(HassBrainSensor(conf[1]))
        if entities:
            await self.component.async_add_entities(entities)

    def get_devices(self):
        """Iterate over devices"""
        lst = []
        for entity in self.component.entities:
            lst.append(entity.name)
        return lst



    async def async_create_device(self, name, state=None):
        """Create a new device."""
        if not name:
            raise ValueError("Name is required")

        # todo this check is important !!!
        # todo reenable
        dev = {
            CONF_NAME: name,
            CONF_STATE: state,
        }

        await self.component.async_add_entities([HassBrainSensor(dev)])
        return dev

    async def async_update_device(self, name, state):
        ent = None
        for entity in self.component.entities:
            if entity.name == name:
                entity.update_state(state)
                entity.device_updated()
                ent = entity
                break
        if ent is None:
            print('~'*10)
            print('name: ', name)
            print('state: ', state)
            print('--')
            for entitiy in self.component.entities:
                print(entitiy.name)
            print('~~')
            raise ValueError
        return ent.to_conf()

    async def async_delete_device(self, name):
        """ Deletes a device by name"""
        ent_reg = await self.hass.helpers.entity_registry.async_get_registry()

        for entity in self.component.entities:
            if entity.name == name:
                await entity.async_remove()
                #ent_reg.async_remove(entity.entity_id)
                break

class HassBrainSensor(Entity):
    """Representation of a Sensor."""

    def __init__(self, conf):
        """Initialize the sensor."""
        self._state = conf[CONF_STATE]
        self._name = conf[CONF_NAME]

    @property
    def name(self):
        """Return the name of the sensor."""
        return self._name

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement."""
        return PERCENT

    def to_conf(self):
        return {CONF_NAME: self._name, CONF_STATE: self._state}

    def update_state(self, new_state):
        self._state = new_state

    def update(self):
        """Fetch new state data for the sensor.

        This is the only method that should fetch new data for Home Assistant.
        """
        #self._state = self.hass.data[DOMAIN]['temp']
        pass


    def device_updated(self):
        """Handle when the config is updated."""
        self._update_state()


    @callback
    def _update_state(self):
        """Update the state."""
        # todo do some stuff here
        self.async_schedule_update_ha_state()

@websocket_api.websocket_command({
    vol.Required('type'): 'hassbrain/device/list',
})
def ws_list_devices(hass: HomeAssistantType,
                    connection: websocket_api.ActiveConnection, msg):
    """List persons."""
    manager = hass.data[DOMAIN]  # type: HassBrainManager
    connection.send_result(msg['id'], {
        'device_list': manager.get_devices(),
    })

@websocket_api.websocket_command({
    vol.Required('type'): 'hassbrain/device/create',
    vol.Required('name'): vol.All(str, vol.Length(min=1)),
    vol.Optional('state'): vol.Any(str, None),
})
# todo check if this works without require admin
@websocket_api.async_response
async def ws_create_device(hass: HomeAssistantType,
                           connection: websocket_api.ActiveConnection, msg):
    """Create a device sensor in the hassbrain component"""
    manager = hass.data[DOMAIN]  # type: HassBrainManager
    try:
        device = await manager.async_create_device(
            name=msg['name'],
            state=msg.get('state')
        )
        connection.send_result(msg['id'], device)
    except ValueError as err:
        connection.send_error(
            msg['id'], websocket_api.const.ERR_INVALID_FORMAT, str(err))


@websocket_api.websocket_command({
    vol.Required('type'): 'hassbrain/device/update',
    vol.Required('name'): vol.All(str, vol.Length(min=1)),
    vol.Required('state'): vol.Any(str, int),
})
@websocket_api.async_response
async def ws_update_device(hass: HomeAssistantType,
                           connection: websocket_api.ActiveConnection, msg):
    """Update a hassbrain device"""
    manager = hass.data[DOMAIN]  # type: HassBrainManager
    changes = {}
    for key in ('name', 'state'):
        if key in msg:
            changes[key] = msg[key]
    try:
        device = await manager.async_update_device(**changes)
        connection.send_result(msg['id'], device)
    except ValueError as err:
        connection.send_error(
            msg['id'], websocket_api.const.ERR_INVALID_FORMAT, str(err))


@websocket_api.websocket_command({
    vol.Required('type'): 'hassbrain/device/delete',
    vol.Required('name'): str,
})
@websocket_api.async_response
async def ws_delete_device(hass: HomeAssistantType,
                           connection: websocket_api.ActiveConnection,
                           msg):
    """Delete a hassbrain device"""
    manager = hass.data[DOMAIN]  # type: HassBrainManager
    await manager.async_delete_device(msg['name'])
    connection.send_result(msg['id'])
